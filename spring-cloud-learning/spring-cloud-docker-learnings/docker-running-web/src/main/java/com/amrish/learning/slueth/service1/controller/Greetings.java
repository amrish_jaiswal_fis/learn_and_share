package com.amrish.learning.slueth.service1.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class Greetings {
	@RequestMapping("/index.html")
	public String sayHello() {
		return "Hello From inside Docker";
	}
}
