package com.amrish.learning.springcloud.springcloudbusexample1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudBusExample1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudBusExample1Application.class, args);
	}

	
}
