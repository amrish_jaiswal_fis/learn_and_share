package com.amrish.learning.springcloud.springcloudconfigclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.amrish.learning.springcloud.springcloudconfigclient.controller.HelloWorldRestController;

@SpringBootApplication(scanBasePackageClasses = {SpringCloudConfigClientApplication.class,HelloWorldRestController.class})
public class SpringCloudConfigClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudConfigClientApplication.class, args);
	}

}
