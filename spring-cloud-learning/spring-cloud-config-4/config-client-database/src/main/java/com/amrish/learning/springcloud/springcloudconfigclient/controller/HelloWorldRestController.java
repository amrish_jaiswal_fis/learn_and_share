package com.amrish.learning.springcloud.springcloudconfigclient.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
@RefreshScope
public class HelloWorldRestController {
	
	@Value("${message}")
	private String messageFromProperty;
	
	@RequestMapping("/message.html")
	public String getMessageFromProperty() {
		return messageFromProperty;
	}
}
