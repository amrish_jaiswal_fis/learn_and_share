package com.amrish.learning.springcloud.springcloudconfigclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.amrish.learning.springcloud.springcloudconfigclient.controller.HelloWorldRestController;

@SpringBootApplication(scanBasePackageClasses = {SpringCloudDatabaseConfigClientApplication.class,HelloWorldRestController.class})
public class SpringCloudDatabaseConfigClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudDatabaseConfigClientApplication.class, args);
	}
	
}
