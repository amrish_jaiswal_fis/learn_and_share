package com.amrish.learning.unittesting.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateTimeConverter {
	private String[][] patternMap = {
			{"[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{1,2}","dd/MM/yy"},
			{"[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{1,2}","MM/dd/yy"}
	};
	
	
	private List<Integer> priorityIndexes;
	public DateTimeConverter(int... priorityIndexes) {
		this.priorityIndexes = new ArrayList<>();
		if(priorityIndexes!=null && priorityIndexes.length>0) {
			for (int i : priorityIndexes) {
				this.priorityIndexes.add(i);
			}
		}
	}
	
	public Date parseDate(String value) {
		Date date=null;
		//check first on priority;
		for (Integer integer : priorityIndexes) {
			date = parseDate(value, integer);
			if(date!=null) {
				break;
			}
		}
		for(int i=0;i<patternMap.length && date==null;i++) {
			if(!priorityIndexes.contains(i) && date==null) {
				date = parseDate(value,i);
			}
		}
		return date;
	}
	
	private Date parseDate(String value,int index) {
		Pattern pattern = Pattern.compile(patternMap[index][0]);
		Matcher matcher = pattern.matcher(value);
		Date dateToReturn = null;
		if(matcher.find()) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(patternMap[index][1]);
			try {
				dateToReturn = simpleDateFormat.parse(value);
			} catch (ParseException e) {
				throw new RuntimeException(e);
			}
		}
		return dateToReturn;
	}
}
