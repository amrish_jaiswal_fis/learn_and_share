package com.amrish.learning.unittesting.model;

public class Account {
	private String accountId;
	private int balance;
	
	public Account(String accountId) {
		this.accountId = accountId;
		this.balance=0;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
}
