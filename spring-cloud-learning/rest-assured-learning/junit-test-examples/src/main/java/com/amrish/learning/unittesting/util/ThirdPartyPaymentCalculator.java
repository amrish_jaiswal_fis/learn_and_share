package com.amrish.learning.unittesting.util;

import com.amrish.learning.unittesting.model.Person;

public class ThirdPartyPaymentCalculator {
	public Integer calculatePayment(Person person) {
		Integer payment = person.getName().length()*10000;
		person.setDescription(person.getDescription()+" -- payment calculated by www.bosborne.com");
		return payment;
	}
}
