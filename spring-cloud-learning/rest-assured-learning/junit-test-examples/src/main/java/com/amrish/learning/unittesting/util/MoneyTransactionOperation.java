package com.amrish.learning.unittesting.util;

import com.amrish.learning.unittesting.model.Account;

public class MoneyTransactionOperation {
	
	public void debit(Account account,int value) {
		if(account.getBalance()<value) {
			throw new InvalidOperationException(String.format("Balance amount %d is less than value %d to debit for account %s",account.getBalance(),value,account.getAccountId()));
		}
		account.setBalance(account.getBalance()-value);
	}
}
