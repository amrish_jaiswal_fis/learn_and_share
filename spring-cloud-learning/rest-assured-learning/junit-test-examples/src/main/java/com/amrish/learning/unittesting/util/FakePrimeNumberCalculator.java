package com.amrish.learning.unittesting.util;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class FakePrimeNumberCalculator {
	public Integer getPrimeNumber()  {
		CountDownLatch latch = new CountDownLatch(1);
		try {
			latch.await(1100, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			System.out.println("An interruption signal received");
		}
		return 5;
	}
}
