package com.amrish.learning.unittesting.extras;

import java.lang.reflect.Method;

import org.junit.jupiter.api.DisplayNameGenerator;

public class DisplayNameGeneratorsBunch {
	public static class ReplaceDigitByWord implements DisplayNameGenerator{

		@Override
		public String generateDisplayNameForClass(Class<?> testClass) {
			return testClass.getCanonicalName();
		}

		@Override
		public String generateDisplayNameForNestedClass(Class<?> nestedClass) {
			return nestedClass.getCanonicalName();
		}

		@Override
		public String generateDisplayNameForMethod(Class<?> testClass, Method testMethod) {
			String name = testMethod.getName();
			name = name.replace("1", "_one")
					.replace("2", "_two")
					.replace("3", "_three")
					.replace("4", "_four")
					.replace("5", "_five")
					.replace("6", "_six")
					.replace("7", "_seven")
					.replace("8", "_eight")
					.replace("9", "_nine");
			return name;
		}
		
	}
}
