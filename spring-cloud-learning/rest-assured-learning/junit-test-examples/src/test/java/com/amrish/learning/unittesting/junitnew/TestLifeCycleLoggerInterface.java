package com.amrish.learning.unittesting.junitnew;

import java.util.logging.Logger;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;

@TestInstance(Lifecycle.PER_METHOD)//required for @BeforeAll and @AfterAll as non-static methods
@Tag("timed")
//@ExtendWith(TimingExtension.class)
public interface TestLifeCycleLoggerInterface {
	public static final Logger logger  = Logger.getLogger("TestLifeCycleLoggerInterface");
	
	@BeforeAll
	public static void staticBeforeAll() {
		logger.info("the static version of before all");		
	}

	@AfterAll
	public static void staticAfterAll() {
		logger.info("the static version of after all");		
	}
	
	@BeforeAll
	public default void nonStaticBeforeAll() {
		logger.info("called before any of the non-static test method is called");
	}

	@AfterAll
	public default void nonStaticAfterAll() {
		logger.info("called before any of the non-static test method is called");
	}
	
	@BeforeEach
	public default void beforeEach() {
		logger.info("Called before each method call");
	}
	
	@AfterEach
	public default void afterEach() {
		logger.info("Called after each method call");
	}

	
}
