package com.amrish.learning.unittesting.junitnew;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class ParameterizedTestExample {
	
	@ParameterizedTest
	@ValueSource(strings = {"one","two","three"})
	public void myParameterizedTest(String arg) {
		assertNotNull(arg);
	}
	
}
