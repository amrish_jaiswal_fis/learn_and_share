package com.amrish.learning.unittesting.util;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.junit.jupiter.api.Assumptions.assumingThat;

import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.api.condition.EnabledIfSystemProperty;
import org.junit.jupiter.api.function.Executable;

import com.amrish.learning.unittesting.extras.DisplayNameGeneratorsBunch;
import com.amrish.learning.unittesting.extras.TestWithSpeedTest;
import com.amrish.learning.unittesting.model.Person;

@DisplayName("The Date Time Conversion test")
@DisplayNameGeneration(DisplayNameGeneratorsBunch.ReplaceDigitByWord.class)
public class DateTimeConvertTest {
	
	private static class DateWithoutDuration{
		int day,month,year;
		DateWithoutDuration(Date date) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			this.day=calendar.get(Calendar.DAY_OF_MONTH);
			this.month=calendar.get(Calendar.MONTH);
			this.year=calendar.get(Calendar.YEAR);
		}
		DateWithoutDuration(int day,int month, int year) {
			this.day=day;
			this.month=month;
			this.year=year;
		}
	}
	
	/**
	 * test it the format dd/MM/yy is supported
	 */
	@Test
	public void testFormat1() {
		DateTimeConverter converter = new DateTimeConverter();
		Date date = converter.parseDate("26/11/20");
		DateWithoutDuration expected = new DateWithoutDuration(26, Calendar.NOVEMBER, 2020);
		DateWithoutDuration actual = new DateWithoutDuration(date);
		assertEquals(expected.day, actual.day);
		assertEquals(expected.month, actual.month);
		assertEquals(expected.year, actual.year);
	}
	
	@Timeout(unit = TimeUnit.MILLISECONDS,value=5)
	@TestWithSpeedTest
	public void testFormat2() {
		DateTimeConverter converter = new DateTimeConverter(1);
		Date date = converter.parseDate("11/26/20");
		DateWithoutDuration expected = new DateWithoutDuration(26, Calendar.NOVEMBER, 2020);
		DateWithoutDuration actual = new DateWithoutDuration(date);
		assertEquals(expected.day, actual.day);
		assertEquals(expected.month, actual.month);
		assertEquals(expected.year, actual.year);
	}
	
	@TestWithSpeedTest
	public void thirdPartyCallTest() {
		Person person = new Person("My Simple Name","My name is FIS Global System");
		ThirdPartyPaymentCalculator calculator = new ThirdPartyPaymentCalculator();
		Integer payment = calculator.calculatePayment(person);
		assertTrue(payment<1000000,()->String.format("The payment %d is beyond limit",payment)) ;
		assertAll("person object intact", 
			()->assertEquals("My Simple Name",person.getName()),
			()->assertEquals("My name is FIS Global System",person.getDescription())
		);
	}
	
	@Test
	public void testExceptionAssertion() {
		assertThrows(ArithmeticException.class, ()->{int x = 1/0; System.out.println("I'm never gonna print " + x);});
	}
	
	@Test
	public void timeNotExceedTest() {
		//-- asserts that the executor gets executed within timespan
		assertTimeout(Duration.ofMillis(1000), ()->{
			new FakePrimeNumberCalculator().getPrimeNumber();
		});
	}
	
	@Test
	public void timeNotExceedPremptively() {
		assertTimeoutPreemptively(Duration.ofMillis(1000), ()->new FakePrimeNumberCalculator().getPrimeNumber());
	}
	
	@Test
	public void testWithAssumption() {
		Integer primeNumber = new FakePrimeNumberCalculator().getPrimeNumber();
		//test that primeNumber is never divisible by 3
		assumeTrue(Objects.nonNull(primeNumber) && primeNumber>3);
		assertNotEquals(primeNumber, (primeNumber/3)*3);
	}

	@Test
	public void testWithAssumption2() {
		Integer primeNumber = new FakePrimeNumberCalculator().getPrimeNumber();
		//test that primeNumber is never divisible by 3
		assumingThat(
				Objects.nonNull(primeNumber) && primeNumber>3, 
				()->assertNotEquals(primeNumber, (primeNumber/3)*3)
		);
	}

	@Test
	public void testWithFailedAssumptionsAlwaysPass() {
		//test that primeNumber is never divisible by 3
		int data=0;
		assumingThat(data!=0,()->assertNotEquals(1, 1));
	}
	
	@Test
	@EnabledIfSystemProperty(named = "encoding",matches = "utf-8")
	public void testOnEncodingCoditionNeverFails() {
		assertEquals("utf-8", System.getProperty("encoding"));
	}
	
}
