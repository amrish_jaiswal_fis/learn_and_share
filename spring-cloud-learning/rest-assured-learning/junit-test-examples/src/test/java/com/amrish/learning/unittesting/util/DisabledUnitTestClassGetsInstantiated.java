package com.amrish.learning.unittesting.util;

import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

//@Disabled // the class will not get instantiated
public class DisabledUnitTestClassGetsInstantiated {
	
	public DisabledUnitTestClassGetsInstantiated() {
		System.out.println("DisabledUnitTestClassGetsInstantiated got instantiated");
	}
	
	@Test
	@Disabled
	public void test() {
		assertFalse(1==1);
	}
}
