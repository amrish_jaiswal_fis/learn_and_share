package com.amrish.learning.unittesting.junitnew;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.TestReporter;

public class RepeatedTestDemo {
	@RepeatedTest(name = "Executing {currentRepetition} out of {totalRepetitions}",value = 10)
	public void repeatTheTest() {
		int x=100;
		assertTrue(()-> x * 1 == 100);
	}
	
	@RepeatedTest(5)
	public void repeatTheTest2(RepetitionInfo repetitionInfo,TestReporter reporter) {
		int x=100;
		assertFalse(()-> x==2);
		reporter.publishEntry("executed times", String.valueOf(repetitionInfo.getCurrentRepetition()));
	}
}
