package com.amrish.learning.unittesting.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.amrish.learning.unittesting.model.Account;

@DisplayName("Nested test on Money transaction")
public class MoneyTransactionNestedTest {
	private Account account;
	
	@BeforeEach
	public void createAccount() {
		account = new Account("amrish");
	}
	
	@Test
	public void testEmpty() {
		assertThrows(InvalidOperationException.class, ()->new MoneyTransactionOperation().debit(account, 100));
	}
	
	@Nested
	public class WhenCredited{
		
		@BeforeEach 
		public void addMoney() {
			account.setBalance(200);
		}
		
		@Test
		public void testDebited() {
			assertFalse(account.getBalance()==0);
		}
		
		@Nested
		public class AfterDebit{
			
			@BeforeEach
			public void debitAmount() {
				new MoneyTransactionOperation().debit(account, 100);				
			}
			
			@Test 
			public void testBalance() {
				assertEquals(100, account.getBalance());
			}
		}
		
	}
}
