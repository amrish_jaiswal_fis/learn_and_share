package com.amrish.learning.unittesting.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;

@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
public class UnitTestPerClass {
	Integer hashCodeOf1;
	Integer hashCodeOf2;
	
	@Test
	public void testShare1() {
		this.hashCodeOf1=this.hashCode();
	}
	
	@Test
	public void testShare2() {
		this.hashCodeOf2=this.hashCode();
		assertEquals(hashCodeOf1,hashCodeOf2);
	}
}
