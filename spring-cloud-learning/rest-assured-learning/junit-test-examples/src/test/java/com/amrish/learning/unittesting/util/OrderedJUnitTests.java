package com.amrish.learning.unittesting.util;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Random;
import java.util.stream.LongStream;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class OrderedJUnitTests {
	private static String str;
	
	@Test
	@Order(1)
	public void testThisFirst() {
		Random random = new Random(10);
		LongStream longStream = random.longs(10, 100000000l);
		long longValue = longStream.findAny().getAsLong();
		assertTrue(longValue>9);
		str = String.valueOf(longValue);
	}
	
	@Test
	@Order(2)
	public void testThisSecond() {
		assertNotNull(str);
		assertTrue(str.length()>1);
	}
}
