package com.amrish.learning.unittesting.junitnew;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestReporter;

@TestInstance(Lifecycle.PER_METHOD)
@DisplayName("Check TestInfo Auto injection")
public class TestInfoInjection {
	
	@Test
	@DisplayName("check whether we get testInfo object")
	//@Tag("one")
	public void testTestInfo(TestInfo testInfo) {
		assertNotNull(testInfo);
		assertTrue(testInfo.getTags().isEmpty());
		testInfo.getTestMethod().ifPresent((m)->assertEquals("testTestInfo",m.getName()));
	}
	
	@Test
	public void testReportParameter(TestReporter reporter) {
		assertNotNull(reporter);
		reporter.publishEntry("This is the custom report entry - entered while testing");
	}
}
