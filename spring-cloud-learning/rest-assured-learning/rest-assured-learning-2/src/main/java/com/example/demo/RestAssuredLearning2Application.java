package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestAssuredLearning2Application {

	public static void main(String[] args) {
		SpringApplication.run(RestAssuredLearning2Application.class, args);
	}

}
