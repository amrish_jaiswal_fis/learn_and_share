package com.amrish.learning.restassured.restassuredlearning1;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import com.amrish.learning.restassured.controller.LoginController;

@SpringBootTest
class SpringBootTestTest {

	@Test
	void contextLoads(@Autowired ApplicationContext applicationContext) {
		assertNotNull(applicationContext);
		LoginController loginController = applicationContext.getBean(LoginController.class);
		assertEquals("FAIL", loginController.login(null, null));
		assertEquals("FAIL", loginController.login(null, ""));
		assertEquals("FAIL", loginController.login("", null));
		assertEquals("FAIL", loginController.login("", ""));
		assertEquals("SUCCESS", loginController.login("first", "first"));
		assertEquals("FAIL", loginController.login("first", null));
		assertEquals("FAIL", loginController.login(null, "first"));
	}
	
	
	

}
