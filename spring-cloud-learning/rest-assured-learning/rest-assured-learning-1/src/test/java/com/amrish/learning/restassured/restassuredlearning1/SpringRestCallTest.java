package com.amrish.learning.restassured.restassuredlearning1;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.amrish.learning.restassured.model.Employee;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
public class SpringRestCallTest {

	@Test
	public void testSingleEntity(@Autowired WebTestClient webTestClient) {
		assertNotNull(webTestClient);
		webTestClient
		.get()
		.uri("/emp/12")
		.exchange()
		.expectStatus()
		.isOk()
		.expectBody(Employee.class)
		.value((c)->System.out.println(c));
		;
	}
	
	@Test
	public void testMultipleEntity(@Autowired WebTestClient webTestClient) {
		assertNotNull(webTestClient);
		webTestClient
		.get()
		.uri("/emp/all")
		.exchange()
		.expectStatus()
		.isOk()
		.expectBody(List.class)
		.value((v)->System.out.println(v));
		;
	}
}
