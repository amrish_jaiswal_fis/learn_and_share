package com.amrish.learning.restassured.restassuredlearning1;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;

import io.restassured.RestAssured;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class RestAssuredTest {
	
	@LocalServerPort
	protected int port;
	
	@Test
	public void testEmployee() {
		RestAssured.port=this.port;
		given().basePath("/emp/12").get().then().statusCode(200).body("id", equalTo("12345"));
	}
}
