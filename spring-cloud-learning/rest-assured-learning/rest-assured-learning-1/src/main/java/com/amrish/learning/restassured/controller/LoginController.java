package com.amrish.learning.restassured.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller("/login/")
public class LoginController {
	private Map<String, String> userMap;
	
	@PostConstruct
	public void initialize() {
		loadUserMap();
	}

	private void loadUserMap() {
		userMap = new LinkedHashMap<String, String>();
		userMap.put("first", "first");
		userMap.put("second", "second");
		userMap.put("third", "third");
		userMap.put("fourth", "fourth");
	}
	
	@RequestMapping(path = "/login.html")
	public String login(@RequestParam("user") String username,@RequestParam("password") String password) {
		String value = userMap.get(username);
		return password!=null&&password.equals(value)?"SUCCESS":"FAIL";
	}
	
}
