package com.amrish.learning.restassured.restassuredlearning1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.amrish.learning.restassured.controller",
		"com.amrish.learning.restassured.restassuredlearning1" })
public class RestAssuredLearning1Application {

	public static void main(String[] args) {
		SpringApplication.run(RestAssuredLearning1Application.class, args);
	}

}
