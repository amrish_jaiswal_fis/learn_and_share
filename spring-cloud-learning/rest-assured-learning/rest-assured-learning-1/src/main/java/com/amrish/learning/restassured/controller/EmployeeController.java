package com.amrish.learning.restassured.controller;

import java.util.ArrayList;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.amrish.learning.restassured.model.Employee;

@RestController
@RequestMapping("/emp/")
public class EmployeeController {
	
	@RequestMapping(path="/{id}",method = RequestMethod.GET,produces = "application/json")
	public Employee getEmployee(@PathParam("id")Integer id) {
		Employee emp = new Employee();
		emp.setId("12345");
		emp.setName("Amrish");
		return emp;
	}
	
	@RequestMapping(path="/all")
	@ResponseBody
	public List<Employee> getAllEmployees(){
		List<Employee> emplist = new ArrayList<Employee>();
		for(int i=0;i<10;i++) {
			Employee emp = new Employee();
			emp.setId(Integer.toString(i+1));
			emp.setName("EMP-"+Integer.toString(i+1));
			emplist.add(emp);
		}
		return emplist;
	}
}
