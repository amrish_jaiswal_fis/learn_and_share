package com.amrish.learning.slueth.service1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.amrish.learning.slueth.service1.controller.ServiceInitiator;
import com.amrish.learning.slueth.service1.controller.SleuthCustomConfiguration;

@SpringBootApplication(scanBasePackageClasses = {Service3Application.class,ServiceInitiator.class,SleuthCustomConfiguration.class})
public class Service3Application {

	public static void main(String[] args) {
		SpringApplication.run(Service3Application.class, args);
	}

}
