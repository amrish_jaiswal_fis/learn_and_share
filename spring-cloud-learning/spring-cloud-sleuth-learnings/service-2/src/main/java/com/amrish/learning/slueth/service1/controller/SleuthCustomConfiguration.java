package com.amrish.learning.slueth.service1.controller;

import org.springframework.cloud.sleuth.instrument.web.HttpClientRequestParser;
import org.springframework.cloud.sleuth.instrument.web.HttpServerRequestParser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import brave.SpanCustomizer;
import brave.http.HttpRequest;
import brave.http.HttpRequestParser;
import brave.propagation.TraceContext;
import brave.sampler.Sampler;

@Configuration
public class SleuthCustomConfiguration {
	
	@Bean(name= {HttpClientRequestParser.NAME,HttpServerRequestParser.NAME})
	public HttpRequestParser createSleuthHttpRequestParser() {
		return new HttpRequestParser() {
			@Override
			public void parse(HttpRequest request, TraceContext context, SpanCustomizer span) {
				span.tag("url", request.url());				
				span.tag("queue-url", request.url());				
			}
		};
	}
	
	@Bean
	public Sampler defaultSampler() {
		return Sampler.ALWAYS_SAMPLE;
	}
}
