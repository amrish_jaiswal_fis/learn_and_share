package com.amrish.learning.springcloudstream.springcloudstream1;

import java.util.function.Function;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringCloudStream1Application {

	public static void main(String[] args) {
		//NOTE see the test class for running
		SpringApplication.run(SpringCloudStream1Application.class, args);
	}

	@Bean
	public Function<String, String> upperCaseBean(){
		return v -> v.toUpperCase();
	}
}
