package com.amrish.learning.springcloud.springcloudconfigclientgit2.model;

public class MessageContainer {
	private String message;
	public MessageContainer(String message) {
		this.message=message;
	}
	public String getMessage() {
		return message;
	}
}
