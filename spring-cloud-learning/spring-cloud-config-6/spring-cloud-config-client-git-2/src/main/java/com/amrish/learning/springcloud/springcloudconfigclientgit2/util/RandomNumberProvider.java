package com.amrish.learning.springcloud.springcloudconfigclientgit2.util;

import java.util.Random;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.annotation.RequestScope;

import com.amrish.learning.springcloud.springcloudconfigclientgit2.model.MessageContainer;

@Configuration
public class RandomNumberProvider {
	private static final Logger logger = LoggerFactory.getLogger(RandomNumberProvider.class);
	
	private Random random;
	private MessageContainer constantMessageContainer;
	
	@PostConstruct
	public void initialize() {
		random = new Random(100);
		this.constantMessageContainer = new MessageContainer(String.valueOf(random.ints().findFirst().getAsInt()));
	}
	
	@RefreshScope
	@Bean(name = "randomValueBean")
	public MessageContainer provideRandomMessageBean() {
		logger.info("Generating new Random value");
		return new MessageContainer(String.valueOf(random.ints(10).findFirst().getAsInt()));
	}
	
	@Bean(name = "constantValueBean")
	@RequestScope
	public MessageContainer constantValue() {
		return constantMessageContainer;
	}
	
}
