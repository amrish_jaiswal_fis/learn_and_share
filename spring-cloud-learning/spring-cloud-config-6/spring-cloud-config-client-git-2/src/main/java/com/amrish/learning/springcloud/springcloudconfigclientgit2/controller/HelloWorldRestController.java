package com.amrish.learning.springcloud.springcloudconfigclientgit2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amrish.learning.springcloud.springcloudconfigclientgit2.model.MessageContainer;

@RestController("/")
@RefreshScope
public class HelloWorldRestController {
	
	@Value("${message}")
	private String messageFromProperty;
	
	@Autowired(required = false)
	@Qualifier("constantValueBean")
	private MessageContainer constantMessage;
	
	@Autowired(required = false)
	@Qualifier("randomValueBean")
	private MessageContainer randomMessage;
	
	@RequestMapping("/message.html")
	public String getMessageFromProperty() {
		//return messageFromProperty;
		return String.format(" Message from Git := %s, Constant Value := %s, Refresh Value := %s", 
					messageFromProperty, constantMessage.getMessage(),randomMessage.getMessage());
	}
}
