package com.amrish.learning.springboot.bootmessagingexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {
		"com.amrish.learning.springboot.bootmessagingexample.controller",
		"com.amrish.learning.springboot.bootmessagingexample"
})
public class BootMessagingExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootMessagingExampleApplication.class, args);
	}

}
