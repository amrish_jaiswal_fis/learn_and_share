package com.amrish.learning.springboot.bootmessagingexample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amrish.learning.springboot.bootmessagingexample.db.EmployeeDbUtil;
import com.amrish.learning.springboot.bootmessagingexample.model.Employee;

@RestController
@RequestMapping("/emp/")
public class EmployeeController {
	@Autowired
	private EmployeeDbUtil employeeDbUtil;
	
	@RequestMapping("/index.html")
	public String sayHello() {
		return "Hello from employee portal";
	}
	
	@RequestMapping(path = "/save",consumes = "application/json")
	public String saveEmployee(@RequestBody Employee employee) {
		employeeDbUtil.insertData(employee);
		return "SUCCESS";
	}

	public EmployeeDbUtil getEmployeeDbUtil() {
		return employeeDbUtil;
	}

	public void setEmployeeDbUtil(EmployeeDbUtil employeeDbUtil) {
		this.employeeDbUtil = employeeDbUtil;
	}
	
}
