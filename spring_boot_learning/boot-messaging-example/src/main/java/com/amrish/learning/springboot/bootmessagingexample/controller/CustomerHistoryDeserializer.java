package com.amrish.learning.springboot.bootmessagingexample.controller;

import java.io.IOException;

import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;

import com.amrish.learning.springboot.bootmessagingexample.model.CustomerHistory;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CustomerHistoryDeserializer implements Deserializer<CustomerHistory>{
	@Override
	public CustomerHistory deserialize(String topic, byte[] data) {
		ObjectMapper mapper = new ObjectMapper();
		CustomerHistory customerHistory = new CustomerHistory();
		try {
			customerHistory = mapper.readValue(data, CustomerHistory.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return customerHistory;
	}
	
	@Override
	public CustomerHistory deserialize(String topic, Headers headers, byte[] data) {
		return Deserializer.super.deserialize(topic, headers, data);
	}
}
