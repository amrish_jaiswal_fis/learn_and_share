package com.amrish.learning.springboot.bootmessagingexample.controller;

import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.amrish.learning.springboot.bootmessagingexample.db.OderDbUtil;
import com.amrish.learning.springboot.bootmessagingexample.model.CustomerHistory;
import com.amrish.learning.springboot.bootmessagingexample.model.ProductSKU;
import com.amrish.learning.springboot.bootmessagingexample.model.PurchaseOrder;

@RestController
@RequestMapping("/product/")
public class OrderController {
	
	@Autowired
	private OderDbUtil orderDbUtil;
	
	@Autowired
	private KafkaTemplate<String, CustomerHistory> kafkaTemplate;
	
	@RequestMapping(path = "/order",consumes = "application/json",method = RequestMethod.POST)
	public String processOrder(@RequestBody PurchaseOrder order) {
		CustomerHistory customerHistory = orderDbUtil.getCustomerHistory(order.getCustomerId());
		ProductSKU purchasedProduct = orderDbUtil.getProductDetails(order.getSkuId());
		customerHistory.getPurchasedItems().add(purchasedProduct);
		ListenableFuture<SendResult<String, CustomerHistory>> future = kafkaTemplate.send("spring-kafka-boot-topic-1", "cust_history", customerHistory);
		try {
			SendResult<String, CustomerHistory> result = future.get();
			ProducerRecord<String,CustomerHistory> producerRecord = result.getProducerRecord();
			Integer partition = producerRecord.partition();
			System.out.println("Partition used " + partition);
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return future.isDone()? "SUCCESS" : "NOT DONE";
	}
}
