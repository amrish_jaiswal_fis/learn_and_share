package com.amrish.learning.springboot.bootmessagingexample.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.amrish.learning.springboot.bootmessagingexample.model.CustomerHistory;
import com.amrish.learning.springboot.bootmessagingexample.model.ProductSKU;

@Repository
public class OderDbUtil {
	@Autowired
	private NamedParameterJdbcTemplate template;	
	
	@Autowired RowMapper<ProductSKU> productSKURowMapper;
	
	/*
	 * @Bean ProductSKUMapper getProductSKUMapper() { return new ProductSKUMapper();
	 * }
	 */
	
	@Component
	public static class ProductSKUMapper implements RowMapper<ProductSKU>{

		@Override
		public ProductSKU mapRow(ResultSet rs, int rowNum) throws SQLException {
			ProductSKU psKU = new ProductSKU();
			psKU.setStockKeepingUnitId(rs.getString("sku_id"));
			psKU.setProductName(rs.getString("p_name"));
			psKU.setDescription(rs.getString("descr"));
			psKU.setStatus(rs.getString("status"));
			return psKU;
		}
		
	}
	
	public ProductSKU getProductDetails(String skuId) {
		final String sql = "select id,sku_id,p_name,descr,status from product_sku where sku_id=:skuId";
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("skuId", skuId);
		List<ProductSKU> productSKU = template.query(sql,mapSqlParameterSource, this.productSKURowMapper);
		return productSKU!=null && !productSKU.isEmpty()?productSKU.get(0):null;
	}
	
	public CustomerHistory getCustomerHistory(String customerId) {
		final String sql = "select product_sku_id from customer_history where customer_id=:customerId";
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("customerId", customerId);
		List<String> skuIds = template.query(sql, mapSqlParameterSource,(rs,rowNum)->rs.getString("product_sku_id"));
		CustomerHistory customerHistory = new CustomerHistory();
		for (String skuId : skuIds) {
			customerHistory.getPurchasedItems().add(getProductDetails(skuId));
		}
		return customerHistory;
	}
}
