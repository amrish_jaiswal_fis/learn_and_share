package com.amrish.learning.springboot.endpoint;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class CustomHealthIndicator implements HealthIndicator{
	private int counter;
	public CustomHealthIndicator() {
		new Thread(new StringValueDumper()).start();
	}
	
	private class StringValueDumper implements Runnable{
		@Override
		public void run() {
			try {
				Thread.sleep(100);
				counter++;
			} catch (InterruptedException e) {
				counter=100;
			}			
		}
	}


	@Override
	public Health health() {
		if(counter>=0 && counter<100) {
			return Health.up().status("200").build();
		}else {
			return Health.down(new RuntimeException("Count reached it's maximum")).build();
		}
	}
}
