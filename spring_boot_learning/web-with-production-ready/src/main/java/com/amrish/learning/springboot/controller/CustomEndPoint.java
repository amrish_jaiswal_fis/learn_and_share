package com.amrish.learning.springboot.controller;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.boot.actuate.endpoint.annotation.Selector.Match;

@Endpoint(enableByDefault = true,id = "customendpoint")
public class CustomEndPoint {
	
	@ReadOperation
	public String getCustomInfo(@Selector(match = Match.ALL_REMAINING) String[] inputs) {
		return "This is a customInfo";
	}
}
