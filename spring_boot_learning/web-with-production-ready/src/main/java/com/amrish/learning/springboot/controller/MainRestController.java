package com.amrish.learning.springboot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
public class MainRestController {
	
	@RequestMapping("/")
	public String indexPage() {
		return "Welcome to my page";
	}
	
	@RequestMapping("/welcome.html")
	public String indexPage(@RequestParam(name = "myname",required = true) String name) {
		return "Welcome " + name + "!!!";
	}
}
