package com.amrish.learning.springboot.kafkaspringbootfirst;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.listener.ContainerProperties;

@SpringBootTest
class KafkaSpringBootFirstApplicationTests {
	private static final Logger logger = LoggerFactory.getLogger(KafkaSpringBootFirstApplicationTests.class);
	@Test
	void contextLoads() {
	}
	
	@Test
	public void testAutocommit() {
		logger.info("Start Auto");
		ContainerProperties containerProperties  = new ContainerProperties("test-topic-1","test-topic-2");
		
	}

}
