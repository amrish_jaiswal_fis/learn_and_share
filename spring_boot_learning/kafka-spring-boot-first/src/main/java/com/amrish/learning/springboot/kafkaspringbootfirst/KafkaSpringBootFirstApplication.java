package com.amrish.learning.springboot.kafkaspringbootfirst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.amrish.learning.springboot.kafkaspringbootfirst.controllers.KafkaMessageProducerController;

@SpringBootApplication(scanBasePackages = {
	"com.amrish.learning.springboot.kafkaspringbootfirst",
	"com.amrish.learning.springboot.kafkaspringbootfirst.controllers"
})
public class KafkaSpringBootFirstApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(KafkaSpringBootFirstApplication.class, args);
		KafkaMessageProducerController controller = context.getBean(KafkaMessageProducerController.class);
		for(int i=0;i<100;i++) {
			controller.sendMessageToKafkaTopic(String.format("Message #%d",i));
			/*
			 * try { Thread.sleep(1000l); } catch (InterruptedException e) { throw new
			 * RuntimeException("Unexpected exception occurred",e); }
			 */
		}
	}

}
