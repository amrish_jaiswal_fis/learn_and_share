package com.amrish.learning.springboot.kafkaspringbootfirst.configs;

import java.util.Arrays;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration("my-own-configuration")
public class BeanConfigs {
	@Bean
	public NewTopic getNewTopic() {
		TopicBuilder topicBuilder = TopicBuilder.name("thing1")
			//.partitions(10)
			//.replicas(5)
			.assignReplicas(0, Arrays.asList(0,1))
			.assignReplicas(1, Arrays.asList(1,2))
			.assignReplicas(2, Arrays.asList(2,3))
			.partitions(3)
			;
		return topicBuilder.build();
	}
	
	
	
	
	
}
