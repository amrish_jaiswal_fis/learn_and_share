package com.amrish.learning.springboot.kafkaspringbootfirst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.amrish.learning.springboot.kafkaspringbootfirst.controllers.KafkaAdminController;

@SpringBootApplication(scanBasePackages = {
	"com.amrish.learning.springboot.kafkaspringbootfirst",
	"com.amrish.learning.springboot.kafkaspringbootfirst.controllers"
})
public class KafkaSpringBootRichApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(KafkaSpringBootRichApplication.class, args);
		KafkaAdminController kafkaAdminController = context.getBean(KafkaAdminController.class);
		kafkaAdminController.examine();
	}

}
