package com.amrish.learning.springboot.kafkaspringbootfirst.controllers;

import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.core.ProducerFactory.Listener;
import org.springframework.kafka.core.ProducerFactoryUtils;
import org.springframework.stereotype.Component;

@Component
public class KafkaAdminController {
	private static final Logger logger  = LoggerFactory.getLogger(KafkaAdminController.class);
	@Autowired
	private KafkaAdmin kafkaAdmin;
	
	@Autowired
	private ProducerFactory<String, String> producerFactory;
	
	public void examine() {
		String supplier = "localhost:9092";
		kafkaAdmin.setBootstrapServersSupplier(()->supplier);
		kafkaAdmin.setAutoCreate(false);
		logger.info("The configuration properties are {}",kafkaAdmin.getConfigurationProperties());
		
		Listener<String, String> listener = new Listener<String,String>(){
			@Override
			public void producerAdded(String id, Producer<String, String> producer) {
				logger.info("Producer having id {} is added {}",id,producer);
			}
			
			public void producerRemoved(String id, Producer<String,String> producer) {
				logger.info("Producer having id {} is removed {}",id,producer);
			};
		};
		producerFactory.addListener(listener);
		System.out.println(producerFactory.transactionCapable());
	}
	
	public void setKafkaAdmin(KafkaAdmin kafkaAdmin) {
		this.kafkaAdmin = kafkaAdmin;
	}

	public void setProducerFactory(ProducerFactory<String, String> producerFactory) {
		this.producerFactory = producerFactory;
	}
	
	
}
