package com.amrish.learning.springboot.actuatorviajmx.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
public class FibonacciiController {
	private List<String> fibonacciiList;
	
	public FibonacciiController() {
		fibonacciiList = new ArrayList<String>();
	}
	
	@RequestMapping("index.html")
	public String indexHtml() {
		return "Hello World";
	}

	@RequestMapping("/next")
	public String getNext() {
		if(fibonacciiList.isEmpty() || fibonacciiList.size()==1) {
			fibonacciiList.add("1");
		}else {
			int lastIndex = fibonacciiList.size()-1;
			String atLast = fibonacciiList.get(lastIndex);
			String atLastMinus1 = fibonacciiList.get(lastIndex-1);
			Integer addition = Integer.parseInt(atLast) + Integer.parseInt(atLastMinus1);
			fibonacciiList.add(String.valueOf(addition));
			
		}
		return String.format("The next fibonacii number is %s", fibonacciiList.get(fibonacciiList.size()-1));
	}

	@RequestMapping("/previous")
	public String getPrevious() {
		if(fibonacciiList.isEmpty()) {
			return "None left!!!";
		}
		return String.format("The previous fibonacii number is %s", fibonacciiList.remove(fibonacciiList.size()-1));
	}
}
