package com.amrish.learning.springboot.actuatorviajmx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.amrish.learning.springboot.actuatorviajmx.controller",
		"com.amrish.learning.springboot.actuatorviajmx"})
public class ActuatorViaJmxApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActuatorViaJmxApplication.class, args);
	}

}
