package com.amrish.learning.springboot.kafkaspringbootfirst.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component("kafkaConsumer")
public class KafkaConsumer {
	private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);
	
	@KafkaListener(topics = {"spring-kafka-topic-1"})
	public void processMessages(String content) {
		logger.info("Message from Kafka Topic - {}", content);
	}
}
