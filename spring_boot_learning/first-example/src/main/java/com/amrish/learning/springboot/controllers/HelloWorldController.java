package com.amrish.learning.springboot.controllers;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;

@Controller
@ConfigurationProperties(prefix = "hwcontroller")
public class HelloWorldController {
	private String defaultValue;	
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	public String getDefault() {
		return StringUtils.isEmpty(getDefaultValue())?"FIS":getDefaultValue();
	}
}
