package com.amrish.learning.springboot.firstexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.amrish.learning.springboot.controllers.HelloWorldController;

@Component
public class PrintHelloWorld {
	@Autowired
	private HelloWorldController helloWorldController;
	public HelloWorldController getHelloWorldController() {
		return helloWorldController;
	}
	public void setHelloWorldController(HelloWorldController helloWorldController) {
		this.helloWorldController = helloWorldController;
	}
	public void printHello(String name) {
		if(StringUtils.isEmpty(name)) {
			System.out.println("Hello " + helloWorldController.getDefault());
		}else {
			System.out.println("Hello " + name);
		}
	}
}
