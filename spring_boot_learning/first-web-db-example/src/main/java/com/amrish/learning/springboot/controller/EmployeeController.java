package com.amrish.learning.springboot.controller;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.amrish.learning.springboot.db.EmployeeDbUtil;
import com.amrish.learning.springboot.model.Employee;

@RestController
@RequestMapping("/emp/")
public class EmployeeController {
	@Autowired
	private EmployeeDbUtil employeeDbUtil;
	
	@RequestMapping(value="/{id}",method = RequestMethod.GET)
	public Employee getEmployeeById(@PathVariable("id") String id) {
		try {
			return employeeDbUtil.getById(Integer.parseInt(id));
		} catch (NumberFormatException e) {
			throw new RuntimeException(e);
		}
	}

	public EmployeeDbUtil getEmployeeDbUtil() {
		return employeeDbUtil;
	}

	public void setEmployeeDbUtil(EmployeeDbUtil employeeDbUtil) {
		this.employeeDbUtil = employeeDbUtil;
	}
}
