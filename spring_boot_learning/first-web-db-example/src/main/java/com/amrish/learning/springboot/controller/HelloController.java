package com.amrish.learning.springboot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
public class HelloController {
	
	@RequestMapping("/")
	public String getDefault() {
		return "The default";
	}
	
	@RequestMapping("index.html")
	public String indexHtml() {
		return "Hello World";
	}
}
