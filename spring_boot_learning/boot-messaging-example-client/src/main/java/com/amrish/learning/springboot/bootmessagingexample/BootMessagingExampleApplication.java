package com.amrish.learning.springboot.bootmessagingexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.amrish.learning.springboot.bootmessagingexample.controller.BaseUrlConfigurationProperties;

@SpringBootApplication(scanBasePackages = {
		"com.amrish.learning.springboot.bootmessagingexample.controller",
		"com.amrish.learning.springboot.bootmessagingexample"
})
@EnableConfigurationProperties(BaseUrlConfigurationProperties.class)
public class BootMessagingExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootMessagingExampleApplication.class, args);
	}

}
