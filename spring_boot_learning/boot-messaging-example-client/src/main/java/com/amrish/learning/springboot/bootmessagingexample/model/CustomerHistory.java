package com.amrish.learning.springboot.bootmessagingexample.model;

import java.util.ArrayList;
import java.util.List;

public class CustomerHistory {
	private String customerId;
	private List<ProductSKU> purchasedItems;
	public CustomerHistory() {
		purchasedItems=new ArrayList<ProductSKU>();
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public List<ProductSKU> getPurchasedItems() {
		return purchasedItems;
	}
	public void setPurchasedItems(List<ProductSKU> purchasedItems) {
		this.purchasedItems = purchasedItems;
	}
}
