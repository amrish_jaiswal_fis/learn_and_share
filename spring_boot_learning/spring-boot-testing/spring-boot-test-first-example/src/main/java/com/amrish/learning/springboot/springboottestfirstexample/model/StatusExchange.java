package com.amrish.learning.springboot.springboottestfirstexample.model;

public class StatusExchange {
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
