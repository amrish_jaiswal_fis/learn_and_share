package com.amrish.learning.springboot.springboottestfirstexample;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItem;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.ObjectContent;

import com.amrish.learning.springboot.springboottestfirstexample.model.ProductSkuDetails;

@JsonTest
public class TestingWithAutoConfigureJSon004 {

	@Autowired
	JacksonTester<ProductSkuDetails> tester;

	@Test
	@DisplayName("Test that serialization works as expected")
	public void test1() throws IOException {
		ProductSkuDetails productSkuDetails = createDummy();
		assertThat(tester).isNotNull();
		/*
		 * String actual = tester.write(productSkuDetails).getJson();
		 * System.out.println(actual);
		 */
		
		assertThat(this.tester.write(productSkuDetails))
			.isEqualToJson("expected-productskudetails.json", this.getClass());

		assertThat(this.tester.write(productSkuDetails)).hasJsonPathStringValue("@.name");
		assertThat(this.tester.write(productSkuDetails))
			.extractingJsonPathStringValue("@.name")
			.isEqualTo("Dalkin AC Flip Inverter 1.5 ton 5 star");
		
		/*
		 * ObjectContent<ProductSkuDetails> objectContent =
		 * tester.read("expected-productskudetails.json"); ProductSkuDetails
		 * expectedObject = objectContent.getObject();
		 * System.out.println(expectedObject.getExpiry());
		 */
		
	}
	
	@Test
	public void deserializationTest() throws IOException {
		String partialData = "{\"sKUId\":\"000000000002\","
				+ "	\"expiry\":\"30/07/2020\",\"name\":\"Quality-premium-Bread\",\"extra\":\"delete\"}";
		assertThat(tester.parse(partialData))
			.hasFieldOrProperty("sKUId")
			.extracting("sKUId").isEqualTo("000000000002");
	}

	private ProductSkuDetails createDummy() {
		ProductSkuDetails productSkuDetails = new ProductSkuDetails();
		productSkuDetails.setsKUId("000000000001");
		productSkuDetails.setName("Dalkin AC Flip Inverter 1.5 ton 5 star");
		productSkuDetails.setDescription("Dalkin AC Flip Inverter 1.5 ton 5 star.. More details");
		productSkuDetails.setCategory("inverter-ac");
		Calendar instance = Calendar.getInstance();
		instance.set(Calendar.YEAR, 2030);
		instance.set(Calendar.MONTH, 11);
		instance.set(Calendar.DATE, 15);
		Date expiry = instance.getTime();
		productSkuDetails.setExpiry(expiry);
		return productSkuDetails;
	}

}
