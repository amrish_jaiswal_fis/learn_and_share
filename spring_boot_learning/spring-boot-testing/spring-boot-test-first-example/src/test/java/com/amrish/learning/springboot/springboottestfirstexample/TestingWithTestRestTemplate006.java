package com.amrish.learning.springboot.springboottestfirstexample;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureMockRestServiceServer;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;

import com.amrish.learning.springboot.springboottestfirstexample.controller.CustomerHistoryProcessorController;
import com.amrish.learning.springboot.springboottestfirstexample.model.StatusExchange;

@RestClientTest(components = {CustomerHistoryProcessorController.class})
//@AutoConfigureWebClient(registerRestTemplate = true)
@AutoConfigureMockRestServiceServer
public class TestingWithTestRestTemplate006 {
	
	@Autowired CustomerHistoryProcessorController customerService;
	
	@Autowired MockRestServiceServer mockServer;
	
	@BeforeEach
	public void initializeMock() {
		this.mockServer.expect(requestTo(endsWith("save_history")))
		.andRespond(withSuccess("{\"status\":\"SUCCESS\"}", MediaType.APPLICATION_JSON));
	}
	
	@Test
	public void testUsingTestRestTemplate() {
		StatusExchange status = customerService.processCustomer(null);
		assertThat(status).isNotNull()
			.hasFieldOrPropertyWithValue("status", "SUCCESS");
	}
}
