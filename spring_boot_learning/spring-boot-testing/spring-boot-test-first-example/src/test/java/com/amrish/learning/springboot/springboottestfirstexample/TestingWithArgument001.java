package com.amrish.learning.springboot.springboottestfirstexample;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.hamcrest.core.IsNot;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.test.context.SpringBootTest;

import com.amrish.learning.springboot.springboottestfirstexample.utils.PalindromeDetector;

@SpringBootTest(args = {"--data.value=radar","--data.value=carac"})
public class TestingWithArgument001 {
	@Autowired PalindromeDetector palindromeDetector;
	
	@Test
	public void testTheArgument(@Autowired ApplicationArguments args) {
		assertThat(args.getOptionNames()).contains("data.value");
		assertNotNull(args.getOptionValues("data.value"));
		assertThat(args.getOptionValues("data.value")).contains("radar");
		List<String> optionValues = args.getOptionValues("data.value");
		System.out.println(optionValues);
		assertNotNull(palindromeDetector);
		for (String optionValuee : optionValues) {
			assertThat(palindromeDetector.isPalindrome(optionValuee)).isEqualTo(true);
		}
	}
}
