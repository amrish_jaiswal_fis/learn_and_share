package com.amrish.learning.springboot.springboottestfirstexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.amrish.learning.springboot.springboottestfirstexample",
		"com.amrish.learning.springboot.springboottestfirstexample.utils"})
public class SpringBootTestFirstExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootTestFirstExampleApplication.class, args);
	}

}
