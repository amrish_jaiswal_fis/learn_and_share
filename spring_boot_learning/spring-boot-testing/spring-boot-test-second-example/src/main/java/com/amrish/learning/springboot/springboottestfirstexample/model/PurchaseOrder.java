package com.amrish.learning.springboot.springboottestfirstexample.model;

public class PurchaseOrder {
	private String customerId;
	private String skuId;
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getSkuId() {
		return skuId;
	}
	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}
}
