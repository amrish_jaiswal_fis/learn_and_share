package com.amrish.learning.springboot.springboottestfirstexample;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockserver.model.HttpRequest.request;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.client.MockServerClient;
import org.mockserver.junit.jupiter.MockServerExtension;
import org.mockserver.junit.jupiter.MockServerSettings;
import org.mockserver.model.HttpResponse;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@ExtendWith(MockServerExtension.class)
@MockServerSettings(ports = {9999})
@SpringBootTest(webEnvironment = WebEnvironment.MOCK)
public class TestWithMockServer003 {
	private MockServerClient client;
	
	@Test
	public void testWithExtension() {
		
	}

	@BeforeEach
	public void createExpectation() {
		client = new MockServerClient("localhost", 9999);
		//client.bind(9999,9898,9888);
		client.when(request()
				.withMethod("GET")
				.withPath("/customer/save_history")
				).respond(HttpResponse.response("{\"status\":\"SUCCESS\"}"));
		
	}
	
	@AfterEach
	public void destroyMockedClient() {
		assertThat(client).isNotNull();
		client.close();
	}
}
