package com.amrish.learning.springboot.springboottestfirstexample;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockserver.model.HttpRequest.request;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.junit.jupiter.MockServerExtension;
import org.mockserver.junit.jupiter.MockServerSettings;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

import com.amrish.learning.springboot.springboottestfirstexample.model.PurchaseOrder;
import com.amrish.learning.springboot.springboottestfirstexample.model.StatusExchange;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressWarnings("unused")
@ExtendWith(MockServerExtension.class)
@MockServerSettings(ports = { 9999 })
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TestRestAssuredWithMockServer002 {
	private ClientAndServer mockServer;
	
	@Autowired private ObjectMapper objectMapper;
	private JacksonTester<StatusExchange> jacksonTester;
	
	@LocalServerPort
	private int port;

	public TestRestAssuredWithMockServer002(ClientAndServer mockServer) {
		this.mockServer = mockServer;
	}

	@BeforeEach
	public void initExpectations() throws IOException {
		JacksonTester.initFields(this, objectMapper);
		mockServer.when(request().withMethod("POST").withPath("/customer/save_history")).respond(HttpResponse.response()
				.withBody("{\"status\":\"SUCCESS\"}").withContentType(MediaType.APPLICATION_JSON));
	}

	@Test
	public void testMyApi() throws JsonProcessingException {
		RestTemplate restTemplate = new RestTemplateBuilder()
					.rootUri(String.format("http://localhost:%d/", port))
					.build();
		
		//String purchaseOrderJsonString = "{\"customerId\":\"cust_123_456\", \"skuId\":\"NRCL12345\"}";
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		purchaseOrder.setCustomerId("cust_123_456");
		purchaseOrder.setSkuId("NRCL12345");
		//ObjectMapper objectMapper = new ObjectMapper();
		//String writeValueAsString = objectMapper.writeValueAsString(purchaseOrder);
		
		StatusExchange statusExchange = restTemplate.postForObject("/after_purchase/save_details", purchaseOrder, StatusExchange.class);
		assertThat(statusExchange).isNotNull()
			.hasFieldOrPropertyWithValue("status", "SUCCESS");
	}

}
