package com.amrish.learning.springboot.springboottestfirstexample;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.junit.jupiter.MockServerExtension;
import org.mockserver.junit.jupiter.MockServerSettings;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.json.JacksonTester;

import com.amrish.learning.springboot.springboottestfirstexample.model.WinningLotto;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.RestAssured;

@SuppressWarnings("unused")
@ExtendWith(MockServerExtension.class)
@MockServerSettings(ports = { 9999 })
@SpringBootTest(webEnvironment = WebEnvironment.MOCK)
public class TestRestAssuredWithMockServer001 {
	private ClientAndServer mockServer;
	@Autowired
	private ObjectMapper objectMapper;
	private JacksonTester<WinningLotto> jacksonTester;

	public TestRestAssuredWithMockServer001(ClientAndServer mockServer) {
		this.mockServer = mockServer;
	}

	@BeforeEach
	public void initExpectations() throws IOException {
		JacksonTester.initFields(this, objectMapper);
		String jsonString = getJsonString();
		assertThat(jsonString).isNotNull();
		this.mockServer.when(HttpRequest.request().withPath("/lotto").withMethod("GET"))
			.respond(
				HttpResponse.response()
				.withBody(jsonString)
				.withContentType(MediaType.APPLICATION_JSON)
				);
	}

	public String getJsonString() {
		String jsonString=null;
		try (InputStream sampleLottoResource = this.getClass().getResourceAsStream("sample-lotto.json")){
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] buffer = new byte[1000];
			int readBytes;
			while((readBytes=sampleLottoResource.read(buffer))!=-1) {
				out.write(buffer,0,readBytes);
			}
			out.flush();
			byte[] byteArray = out.toByteArray();
			jsonString = new String(byteArray);
		}catch(IOException e) {
			throw new RuntimeException(e);
		}
		return jsonString;
	}

	@Test
	public void testRestAssured1() {
		assertNotNull(jacksonTester);
		RestAssured.get("http://localhost:9999/lotto").then().body("lotto.lottoId", equalTo(5));
	}

	@Test
	public void testRestAssured2() {
		assertNotNull(jacksonTester);
		RestAssured.get("http://localhost:9999/lotto").then().body("lotto.winningNumbers", hasItems(2,5));
	}
}
