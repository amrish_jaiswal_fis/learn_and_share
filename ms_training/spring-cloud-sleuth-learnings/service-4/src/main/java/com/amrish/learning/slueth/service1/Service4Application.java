package com.amrish.learning.slueth.service1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.amrish.learning.slueth.service1.controller.ServiceInitiator;
import com.amrish.learning.slueth.service1.controller.SleuthCustomConfiguration;

@SpringBootApplication(scanBasePackageClasses = {Service4Application.class,ServiceInitiator.class,SleuthCustomConfiguration.class})
public class Service4Application {

	public static void main(String[] args) {
		SpringApplication.run(Service4Application.class, args);
	}

}
