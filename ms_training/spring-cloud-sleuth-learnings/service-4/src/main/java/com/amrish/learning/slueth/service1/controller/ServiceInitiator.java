package com.amrish.learning.slueth.service1.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.SpanName;
import org.springframework.cloud.sleuth.annotation.ContinueSpan;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import brave.Span;
import brave.Tracer;
import brave.propagation.ExtraFieldPropagation;

@RestController
@RequestMapping("/start")
public class ServiceInitiator {
	private static final Logger logger = LoggerFactory.getLogger(ServiceInitiator.class); 
	
	@Autowired(required = false)
	private Tracer tracer;
	
	@RequestMapping("/callservice4")
	public String callService4() {
		Integer result = callSomeBusinessLogic();
		String numberInText = printAsString(result);
		return numberInText;
	}

	@SpanName("callSomeBusinessLogic")
	@NewSpan(name = "called-some-business-logic")
	public Integer callSomeBusinessLogic() {
		logger.info("Ok! For you I will return the highest prime number less than 10000");
		int primeNumber = 1;
		boolean primeNumberConfirmed;
		for(int i=10000;i>=1;i--) {
			primeNumber = i;
			primeNumberConfirmed=true;
			for(int j=primeNumber/2;j>1;j--) {
				if(i%j==0) {
					primeNumberConfirmed=false;
					break;
				}
			}
			if(primeNumberConfirmed) {
				break;
			}
		}
		logger.info("returning value {}",primeNumber);
		return primeNumber;
	}
	
	@ContinueSpan(log = "printAsString-called")
	public String printAsString(@SpanTag(key = "prime-number") int input) {
		StringBuilder stb = new StringBuilder();
		char[] inputToProcess = String.valueOf(input).toCharArray();
		for(int i=0;i<inputToProcess.length;i++) {
			stb.append(String.format("%s ",getWordFromDigit(inputToProcess[i])));
		}
		return stb.toString();
	}
	
	@SuppressWarnings("deprecation")
	public String getWordFromDigit(@SpanTag(key="digit-convert",value = "into words") char digit) {
		Span newSpan = this.tracer.nextSpan();
		newSpan.start();
		String numberInString=null;
		switch(digit) {
			case '1': numberInString="ONE"; break;
			case '2': numberInString="TWO"; break;
			case '3': numberInString="THREE"; break;
			case '4': numberInString="FOUR"; break;
			case '5': numberInString="FIVE"; break;
			case '6': numberInString="SIX"; break;
			case '7': numberInString="SEVEN"; break;
			case '8': numberInString="EIGHT"; break;
			case '9': numberInString="NINE"; break;
			case '0': numberInString="ZERO"; break;
		}
		if(tracer!=null) {
			logger.info("adding new span manually with baggage");
			newSpan.annotate("when the digit to work is called");
			ExtraFieldPropagation.set(this.tracer.currentSpan().context(), "word-to-digit", numberInString);
			newSpan.finish(10000);
		}
		return numberInString;
	}
	
}
