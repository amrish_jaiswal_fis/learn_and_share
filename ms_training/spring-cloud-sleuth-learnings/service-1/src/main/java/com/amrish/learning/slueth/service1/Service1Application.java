package com.amrish.learning.slueth.service1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.amrish.learning.slueth.service1.controller.ServiceInitiator;
import com.amrish.learning.slueth.service1.controller.SleuthCustomization;

@SpringBootApplication(scanBasePackageClasses = {Service1Application.class,ServiceInitiator.class,SleuthCustomization.class})
public class Service1Application {

	public static void main(String[] args) {
		SpringApplication.run(Service1Application.class, args);
	}

}
