package com.amrish.learning.slueth.service1.controller;

import java.util.Random;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.sleuth.SpanName;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/start")
public class ServiceInitiator {
	private static final Logger logger = LoggerFactory.getLogger(ServiceInitiator.class); 
	
	private Random random;
	
	private final RestTemplate restTemplate;
	
	public ServiceInitiator(@Value("${service4.baseurl}") String serviceBaseUrl
			,@Autowired RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.rootUri(serviceBaseUrl)
		.build();
	}
	
	@PostConstruct
	public void init() {
		random = new Random();
	}
	
	@RequestMapping("/callservice3")
	@SpanName("the-call-service-3")
	public String callService3() {
		String result = "SUCCESS from Service3";
		int randomBetween0And10 = random.nextInt(10);
		int randomBetween1And10 = (randomBetween0And10+1)%10;
		long sleepTime = randomBetween1And10*10000;
		String resultFromServiceCall = this.restTemplate.getForObject("/start/callservice4", String.class);
		logger.info("Sleeping for time {} milliseconds",sleepTime);
		logger.info("result for service4 : {}",resultFromServiceCall);
		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException e) {
			logger.error("An interruptedException received",e);
			throw new RuntimeException(e);
		}
		return result;
	}
	
}
