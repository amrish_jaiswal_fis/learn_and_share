1. Go to docker-running-web
2. Below command will build the application
docker build -f .\Dockerfile -t local-dockers/docker-running-web:1.0.1-SNAPSHOT .\target
3. After completing the build, you can verify the image being created using below command
docker images
4. Use below command for executing application as docker image
docker run --name docker-running-web-container --rm -p 8084:8084 local-dockers/docker-running-web:1.0.1-SNAPSHOT
5. Execute below command to verify application is running-web
docker ps
curl http://localhost:8084/greetings
6. Execute below command to get the container id
docker ps
The tablular data would be printed with columns "CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS      "
copy the container id
execute below command to open shell inside docker unix environment.

docker exec -it <container-id> /bin/sh

For example:-
docker exec -it 910b9e1390ba  /bin/sh

## You can see what is inside

7. execute below command to come out of the container shell
exit

8. Execute below command to see the memory & CPU usage of your docker
docker stats 
(Alternatively you can also use "docker container stats <container_id>"
Use Ctrl+C to come out of the docker stats window

9. Finally, to stop contianer use below command
docker container stop <container-id>
docker container stop 910b9e1390ba

10. Please visit https://docs.docker.com/engine/reference/run/ for docker tool manual

11. Please visit https://docs.docker.com/engine/reference/commandline/dockerd/, for knowing how you can modify server environment