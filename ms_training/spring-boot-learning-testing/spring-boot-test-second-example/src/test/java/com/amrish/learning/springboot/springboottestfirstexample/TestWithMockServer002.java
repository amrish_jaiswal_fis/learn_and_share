package com.amrish.learning.springboot.springboottestfirstexample;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.junit.jupiter.MockServerExtension;
import org.mockserver.junit.jupiter.MockServerSettings;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import com.amrish.learning.springboot.springboottestfirstexample.model.ProductSkuDetails;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

@ExtendWith(MockServerExtension.class)
@MockServerSettings(ports = {9999})
@SpringBootTest(webEnvironment = WebEnvironment.MOCK)
public class TestWithMockServer002 {
	private final ClientAndServer mockServer;

	private ObjectMapper objectMapper;
	
	public TestWithMockServer002(ClientAndServer mockAndServer) {
		this.mockServer=mockAndServer;
		this.objectMapper=new ObjectMapper();
	}
	
	@Test
	public void testWithExtension() {
		assertThat(mockServer).isNotNull();
	}
}
