package com.amrish.learning.springboot.springboottestfirstexample;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockserver.model.HttpRequest.request;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.junit.jupiter.MockServerExtension;
import org.mockserver.junit.jupiter.MockServerSettings;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.MediaType;
import org.mockserver.verify.VerificationTimes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import com.amrish.learning.springboot.springboottestfirstexample.controller.CustomerHistoryProcessorController;
import com.amrish.learning.springboot.springboottestfirstexample.model.PurchaseOrder;
import com.amrish.learning.springboot.springboottestfirstexample.model.StatusExchange;

@ExtendWith(MockServerExtension.class)
@MockServerSettings(ports = { 9999 })
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestWithMockServer005 {
	private ClientAndServer mockServer;
	
	@Autowired CustomerHistoryProcessorController customerHistoryController;
	
	public TestWithMockServer005(ClientAndServer mockServer) {
		this.mockServer = mockServer;
	}

	@Test
	@Order(1)
	public void testWithExtension() {
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		purchaseOrder.setCustomerId("anything");
		purchaseOrder.setSkuId("anything");
		StatusExchange status = customerHistoryController.processCustomer(purchaseOrder);
		assertThat(status).isNotNull();
		assertThat(status).hasFieldOrPropertyWithValue("status", "SUCCESS");
	}

	@Test
	@Order(2)
	public void verifyReceivedRequest() {
		this.mockServer.verify(HttpRequest.request().withMethod("POST").withPath("/customer/save_history"),
				VerificationTimes.atLeast(1));
	}

	@BeforeEach
	public void createExpectation() {
		mockServer.when(request().withMethod("POST").withPath("/customer/save_history")).respond(HttpResponse.response()
				.withBody("{\"status\":\"SUCCESS\"}").withContentType(MediaType.APPLICATION_JSON));

	}

}
