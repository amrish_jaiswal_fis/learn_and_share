package com.amrish.learning.springboot.springboottestfirstexample;

import static org.junit.jupiter.api.Assumptions.assumeTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.netty.MockServer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@SpringBootTest(webEnvironment = WebEnvironment.MOCK)
public class TestWithMockServer001 {
	private static ClientAndServer mockServer;
	@BeforeAll
	public static void initialize() {
		mockServer = ClientAndServer.startClientAndServer(8080);
	}
	
	@AfterAll
	public static void destroy() {
		assumeTrue(mockServer!=null);
		mockServer.close();
	}
	
	@Test
	public void testFirstServer() {
		//doing nothing
	}
}
