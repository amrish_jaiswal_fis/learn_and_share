package com.amrish.learning.springboot.springboottestfirstexample.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ProductSkuDetails {
	private String sKUId;
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date expiry;
	private String name;
	private String category;
	private String description;
	public String getsKUId() {
		return sKUId;
	}
	public void setsKUId(String sKUId) {
		this.sKUId = sKUId;
	}
	public Date getExpiry() {
		return expiry;
	}
	public void setExpiry(Date expiry) {
		this.expiry = expiry;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
