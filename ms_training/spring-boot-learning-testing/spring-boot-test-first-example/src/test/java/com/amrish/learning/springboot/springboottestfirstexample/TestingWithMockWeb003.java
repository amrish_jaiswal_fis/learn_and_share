package com.amrish.learning.springboot.springboottestfirstexample;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.amrish.learning.springboot.springboottestfirstexample.utils.PalindromeDetector;

@SpringBootTest
public class TestingWithMockWeb003 {
	@Autowired PalindromeDetector palindromeDetector;
	
	@Test
	@DisplayName("Test that mock application is launched")
	public void testValid() {
		assertThat(palindromeDetector).isNotNull();
	}
}
