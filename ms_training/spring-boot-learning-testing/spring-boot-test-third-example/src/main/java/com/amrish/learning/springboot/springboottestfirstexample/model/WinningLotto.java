package com.amrish.learning.springboot.springboottestfirstexample.model;

public class WinningLotto {
	private Lotto lotto;

	public Lotto getLotto() {
		return lotto;
	}

	public void setLotto(Lotto lotto) {
		this.lotto = lotto;
	}
}
