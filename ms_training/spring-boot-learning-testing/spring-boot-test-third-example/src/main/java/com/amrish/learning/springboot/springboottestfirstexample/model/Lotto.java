package com.amrish.learning.springboot.springboottestfirstexample.model;

import java.util.List;

public class Lotto {
	private String lottoId;
	private String winningNumbers;
	private List<Winners> winners;
	public String getLottoId() {
		return lottoId;
	}
	public void setLottoId(String lottoId) {
		this.lottoId = lottoId;
	}
	public String getWinningNumbers() {
		return winningNumbers;
	}
	public void setWinningNumbers(String winningNumbers) {
		this.winningNumbers = winningNumbers;
	}
	public List<Winners> getWinners() {
		return winners;
	}
	public void setWinners(List<Winners> winners) {
		this.winners = winners;
	}
}
