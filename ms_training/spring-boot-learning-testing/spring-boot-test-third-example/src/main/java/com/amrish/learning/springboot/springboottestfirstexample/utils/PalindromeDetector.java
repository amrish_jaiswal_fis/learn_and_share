package com.amrish.learning.springboot.springboottestfirstexample.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class PalindromeDetector {
	private static final Logger logger = LoggerFactory.getLogger(PalindromeDetector.class);
	public boolean isPalindrome(String input) {
		logger.info("checking palindrome for {}",input);
		boolean palindrome=true;
		char[] charArray = input.toCharArray();
		for(int i=0;i<(charArray.length/2);i++) {
			if(charArray[i]!=charArray[charArray.length-1-i]) {
				palindrome=false;
				logger.info("charArray[%d]!=charArray[%d] hence it is not palindrome",charArray[i],charArray[charArray.length-1-i]);
				break;
			}
		}
		return palindrome;
	}
}
