package com.amrish.learning.springboot.springboottestfirstexample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.amrish.learning.springboot.springboottestfirstexample.model.PurchaseOrder;
import com.amrish.learning.springboot.springboottestfirstexample.model.StatusExchange;

@RestController
@RequestMapping("/after_purchase/")
public class CustomerHistoryProcessorController {
	
	private final RestTemplate restTemplate;
	
	public CustomerHistoryProcessorController(@Autowired RestTemplateBuilder restTemplateBuilder,
			@Value("${customer.service.url}") String baseUrl) {
		this.restTemplate = restTemplateBuilder.rootUri(baseUrl).build();
	}
	
	@RequestMapping(path = "/sayhello.html")
	public String greetings() {
		return "Hello";
	}
	
	@RequestMapping(path = "/save_details",method = RequestMethod.POST,produces = "application/json")
	public StatusExchange processCustomer(@RequestBody PurchaseOrder purchaseOrder) {
		StatusExchange statusExchange = this.restTemplate.postForObject("/save_history",purchaseOrder , StatusExchange.class);
		//some logic to verify
		return statusExchange;
	}
}
