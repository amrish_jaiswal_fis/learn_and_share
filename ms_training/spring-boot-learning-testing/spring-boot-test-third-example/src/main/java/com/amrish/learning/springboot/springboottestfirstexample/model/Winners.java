package com.amrish.learning.springboot.springboottestfirstexample.model;

public class Winners {
	private String winnerId;
	private String[] numbers;
	public String getWinnerId() {
		return winnerId;
	}
	public void setWinnerId(String winnerId) {
		this.winnerId = winnerId;
	}
	public String[] getNumbers() {
		return numbers;
	}
	public void setNumbers(String[] numbers) {
		this.numbers = numbers;
	}
}
