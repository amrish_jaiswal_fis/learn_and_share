package com.examples.spring.cloud.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableDiscoveryClient
@RestController
public class SpringCloudServiceRegistration {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudServiceRegistration.class, args);
	}

	@GetMapping("/")
	public String greeting()
	{
		return "Welcome to Product Service API";
	}

}
