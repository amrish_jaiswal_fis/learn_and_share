package com.amrish.learning.springcloud.springcloudconfigclientgit2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.amrish.learning.springcloud.springcloudconfigclientgit2.controller","com.amrish.learning.springcloud.springcloudconfigclientgit2"})
public class SpringCloudConfigClientGit2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudConfigClientGit2Application.class, args);
	}

}
