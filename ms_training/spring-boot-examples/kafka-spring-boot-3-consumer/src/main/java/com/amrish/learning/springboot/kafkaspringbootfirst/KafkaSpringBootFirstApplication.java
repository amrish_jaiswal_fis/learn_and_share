package com.amrish.learning.springboot.kafkaspringbootfirst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {
	"com.amrish.learning.springboot.kafkaspringbootfirst",
	"com.amrish.learning.springboot.kafkaspringbootfirst.controllers"
})
public class KafkaSpringBootFirstApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaSpringBootFirstApplication.class, args);
	}

}
