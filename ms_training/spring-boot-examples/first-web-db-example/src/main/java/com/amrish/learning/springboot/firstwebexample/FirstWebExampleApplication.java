package com.amrish.learning.springboot.firstwebexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.amrish.learning.springboot","com.amrish.learning.springboot.controller"})
public class FirstWebExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstWebExampleApplication.class, args);
	}

}
