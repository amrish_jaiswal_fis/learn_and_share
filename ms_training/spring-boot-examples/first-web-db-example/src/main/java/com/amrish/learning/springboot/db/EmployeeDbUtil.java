package com.amrish.learning.springboot.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Component;

import com.amrish.learning.springboot.model.Employee;

@Component
public class EmployeeDbUtil {
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("employeeRowMapper")
	private RowMapper<Employee> rowMapper;
	
	@Bean(name = "employeeRowMapper")
	public EmployeeRowMapper provideEmployeeRowMapperBean() {
		return new EmployeeRowMapper();
	} 

	private class EmployeeRowMapper implements RowMapper<Employee>{
		@Override
		public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
			Employee employee = new Employee();
			employee.setId(rs.getInt("EMP_ID"));
			employee.setName(rs.getString("EMP_NAME"));
			employee.setDescription(rs.getString("EMP_DESC"));
			employee.setSalary(rs.getDouble("sal"));
			return employee;
		}
	}
	
	public Employee getById(Integer id) {
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("empId", id);
		List<Employee> employeeList = jdbcTemplate.query("select emp_id,emp_name,emp_desc,sal from employee where emp_id=:empId",mapSqlParameterSource,this.rowMapper);
		return employeeList!=null && !employeeList.isEmpty()?employeeList.get(0):null;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public NamedParameterJdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public RowMapper<Employee> getRowMapper() {
		return rowMapper;
	}

	public void setRowMapper(RowMapper<Employee> rowMapper) {
		this.rowMapper = rowMapper;
	}

	
}
