package com.amrish.learning.springboot.firstexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication(scanBasePackages = {"com.amrish.learning.springboot.controllers","com.amrish.learning.springboot"})
public class FirstExampleApplication {
	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(FirstExampleApplication.class, args);
		System.out.println(applicationContext);
		PrintHelloWorld printHelloWorld = applicationContext.getBean(PrintHelloWorld.class);
		printHelloWorld.printHello(null);
		printHelloWorld.printHello("Friends!!");
		
	}
}
