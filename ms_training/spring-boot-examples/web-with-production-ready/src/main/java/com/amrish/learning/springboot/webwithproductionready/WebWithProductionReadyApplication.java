package com.amrish.learning.springboot.webwithproductionready;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.amrish.learning.springboot.controller.CustomEndPoint;
import com.amrish.learning.springboot.controller.MainRestController;
import com.amrish.learning.springboot.endpoint.CustomHealthIndicator;
import com.amrish.learning.springboot.endpoint.CustomInfoProvider;

@SpringBootApplication(scanBasePackageClasses = {MainRestController.class,CustomEndPoint.class,CustomHealthIndicator.class,CustomInfoProvider.class})
public class WebWithProductionReadyApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebWithProductionReadyApplication.class, args);
	}

}
