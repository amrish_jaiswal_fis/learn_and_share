package com.amrish.learning.springboot.endpoint;

import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.Info.Builder;
import org.springframework.stereotype.Component;

@Component
public class CustomInfoProvider implements InfoContributor{
	@Override
	public void contribute(Builder builder) {
		Info build = builder.withDetail("name", "Amrish").withDetail("Company", "FIS").build();
		System.out.println(build);		
	}
}
