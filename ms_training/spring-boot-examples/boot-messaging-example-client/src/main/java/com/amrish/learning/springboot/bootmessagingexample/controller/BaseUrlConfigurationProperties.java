package com.amrish.learning.springboot.bootmessagingexample.controller;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "order.base",ignoreInvalidFields = true,ignoreUnknownFields = true)
@Configuration
public class BaseUrlConfigurationProperties {
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}
