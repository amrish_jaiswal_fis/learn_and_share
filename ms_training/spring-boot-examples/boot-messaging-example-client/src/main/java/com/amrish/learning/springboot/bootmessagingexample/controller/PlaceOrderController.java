package com.amrish.learning.springboot.bootmessagingexample.controller;

import java.time.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.amrish.learning.springboot.bootmessagingexample.model.PurchaseOrder;

@RestController
@RequestMapping("/product/")
public class PlaceOrderController {
	private static final Logger logger = LoggerFactory.getLogger(PlaceOrderController.class);
	private RestTemplate restTemplate;
	
	public PlaceOrderController(@Autowired RestTemplateBuilder restTemplateBuilder,@Value("${order.base.uri}") String rootURI,
			@Autowired BaseUrlConfigurationProperties baseUrlConfigurationProperties) {
		init(restTemplateBuilder,rootURI,baseUrlConfigurationProperties);
	}
	
	public void init(RestTemplateBuilder restTemplateBuilder,String rootURI,
			BaseUrlConfigurationProperties baseUrlConfigurationProperties) {
		//showing two ways of getting base uri
		logger.info("rootURI : {}",rootURI);
		logger.info("baseUrlConfigurationProperties : {}", baseUrlConfigurationProperties);
		logger.info("baseUrlConfigurationProperties URI : {}", baseUrlConfigurationProperties.getUrl());
		this.restTemplate = restTemplateBuilder
			.setConnectTimeout(Duration.ofMinutes(3))
			.rootUri(baseUrlConfigurationProperties.getUrl())
			.build();
	}
	
	@RequestMapping(path = "/place",method = RequestMethod.GET)
	public String placeOrder(String orderId) {
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		purchaseOrder.setCustomerId("cust1");
		purchaseOrder.setSkuId("psku_6");
		String result = restTemplate.postForObject("/product/order", purchaseOrder, String.class);
		logger.info("result received : {}",result);
		return "SUCCESS";
	}

	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
}
