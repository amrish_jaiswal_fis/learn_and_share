package com.amrish.learning.springboot.bootmessagingexample.model;

public class ProductSKU {
	private String stockKeepingUnitId;
	private String productName;
	private String description;
	private String status;
	public String getStockKeepingUnitId() {
		return stockKeepingUnitId;
	}
	public void setStockKeepingUnitId(String stockKeepingUnitId) {
		this.stockKeepingUnitId = stockKeepingUnitId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
