package com.amrish.learning.springboot.bootmessagingexample.controller;

import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amrish.learning.springboot.bootmessagingexample.model.CustomerHistory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CustomerHistorySerializer implements Serializer<CustomerHistory>{
	private static final Logger logger = LoggerFactory.getLogger(CustomerHistorySerializer.class);
	
	@Override
	public byte[] serialize(String topic, CustomerHistory data) {
		//let's convert it into JSON.
		ObjectMapper mapper = new ObjectMapper();
		try {
			String jsonString = mapper.writeValueAsString(data);
			logger.info("serialized json string {}",jsonString);
			return jsonString.getBytes();
		} catch (JsonProcessingException e) {
			logger.error("exception occurred",e);
		}
		return null;
	}
}
